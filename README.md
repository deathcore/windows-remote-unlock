# Windows Remote Unlock

Script to disconnect from an RDP session without locking the remote PC.

(Useful for remote game streaming such as Steam Link/Moonlight)


Usage:
1.  RDP onto remote machine in question.
1. Run DisconnectRDP.bat as Administrator.
1. Your RDP session should disconnect and Remote PC should be left unlocked.
